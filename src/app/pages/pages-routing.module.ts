import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Component pages
import { DashboardComponent } from "./dashboard/dashboard.component";
import { BudgetInputComponent } from './budget-input/budget-input.component';
import { ProdplanComponent } from './prodplan/prodplan.component';
import { UsersComponent } from './master/users/users.component';
import { CostCenterComponent } from './master/cost-center/cost-center.component';
import { CalculationBudgetComponent } from './master/calculation-budget/calculation-budget.component';
import { MaterialComponent } from './master/material/material.component';
import { DetailMaterialComponent } from './master/material/detail-material/detail-material.component';
import { LineComponent } from './master/line/line.component';
import { RoleGuard } from '../core/guards/role.guard';
import { DetailSupplyComponent } from './detail-supply/detail-supply.component';
import { OpexComponent } from './master/opex/opex.component';
import { OpexTransactionComponent } from './opex-transaction/opex-transaction.component';
import { OpexDashboardComponent } from './opex-dashboard/opex-dashboard.component';
import { OpexFixComponent } from './master/opex-fix/opex-fix.component';
import { ActualTransactionComponent } from './actual-transaction/actual-transaction.component';

const routes: Routes = [
    {
        path: "",
        component: DashboardComponent
    },
    {
      path: "supplies",
      component: BudgetInputComponent
    },
    {
      path: "supplies/detail/:code",
      component: DetailSupplyComponent
    },
    {
      path: "actual-transactions",
      component: ActualTransactionComponent
    },
    {
      path: "prodplan",
      component: ProdplanComponent
    },
    {
      path: "dashboard-opx",
      component: OpexDashboardComponent
    },
    {
      path: "opex",
      component: OpexTransactionComponent
    },
    {
      path: "master/material",
      component: MaterialComponent,
      canActivate: [RoleGuard],
      data: { expectedRole: "Admin" }
    },
    {
      path: "master/material/:code",
      component: DetailMaterialComponent,
      canActivate: [RoleGuard],
      data: { expectedRole: "Admin" }
    },
    {
      path: "master/order-opex",
      component: OpexComponent,
      canActivate: [RoleGuard],
      data: { expectedRole: "Admin" }
    },
    {
      path: "master/opex",
      component: OpexFixComponent,
      canActivate: [RoleGuard],
      data: { expectedRole: "Admin" }
    },
    {
      path: "master/cost-center",
      component: CostCenterComponent,
      canActivate: [RoleGuard],
      data: { expectedRole: "Admin" }
    },
    {
      path: "master/factory-line",
      component: LineComponent,
      canActivate: [RoleGuard],
      data: { expectedRole: "Admin" }
    },
    {
      path: "master/calc-budget",
      component: CalculationBudgetComponent,
      canActivate: [RoleGuard],
      data: { expectedRole: "Admin" }
    },
    {
      path: "master/users",
      component: UsersComponent,
      canActivate: [RoleGuard],
      data: { expectedRole: "Admin" }
    },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
