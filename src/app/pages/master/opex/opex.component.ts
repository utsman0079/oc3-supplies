import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject, debounceTime } from 'rxjs';
import { CommonService } from 'src/app/core/services/common.service';
import { restApiService } from 'src/app/core/services/rest-api.service';
import { Const } from 'src/app/core/static/const';
import { GlobalComponent, SortDirection } from 'src/app/global-component';

@Component({
  selector: 'app-opex',
  templateUrl: './opex.component.html',
  styleUrls: ['./opex.component.scss']
})
export class OpexComponent {
  opexData: any[] = []
  opexId!: number | null

  isLoading = false;
  breadCrumbItems: Array<{}>

  searchTerm = ''
  searchSubject = new Subject<string>()

  totalItems = 0
  currentPage = 1
  pageSize = 10
  totalPages!: number
  sortColumn = 'order'
  sortDirection: SortDirection = 'asc'

  form = {
    order: '',
    description: '',
    type: '',
    coar: ''
  }
  isFormInvalid = false;

  opexXlsxLink = GlobalComponent.API_URL + `file/xlsx/opex`

  constructor(private apiService: restApiService, private modalService: NgbModal, public common: CommonService) {
    this.breadCrumbItems = [
      { label: 'Master', active: false },
      { label: 'OPEX', active: true }
    ];
    this.searchSubject.pipe(debounceTime(350)).subscribe((term) => {
      this.searchOpexByPagination(term, this.currentPage, this.pageSize)
    })
  }

  ngOnInit() {
    this.searchOpexByPagination(this.searchTerm, this.currentPage, this.pageSize)
  }

  searchOpexByPagination(term: string, currentPage: number, pageSize: number) {
    this.apiService.searchOpexByPagination(term, currentPage, pageSize, this.sortColumn, this.sortDirection).subscribe({
      next: (res) => {
        this.opexData = res.data
        this.totalItems = res.total_opex
      },
      error: (err) => this.common.showServerErrorAlert(Const.ERR_GET_MSG("OPEX"), err)
    })
  }

  calculateStartingIndex(index: number): number {
    return (this.currentPage - 1) * this.pageSize + index + 1;
  }

  openModal(template: any, data?: any) {
    if (data) {
      this.opexId = data.id
      Object.keys(this.form).forEach(key => (this.form as any)[key] = data[key])
    }
    this.modalService.open(template, { centered: true, size: 'lg' }).result.then(
      (result) => this.resetModalValue(),
      (reason) => this.resetModalValue()
    )
  }

  resetModalValue() {
    Object.keys(this.form).forEach(key => (this.form as any)[key] = '')
    this.opexId = null
    this.isFormInvalid = false
  }

  onDeleteOpex(id: number) {
    this.common.showDeleteWarningAlert().then(result => {
      if (result.isConfirmed) {
        this.apiService.updateOpex(id, { is_removed: 1 }).subscribe({
          next: (res) => {
            this.searchSubject.next(this.searchTerm)
            this.modalService.dismissAll()
          },
          error: (err: HttpErrorResponse) => {
            this.common.showErrorAlert(Const.ERR_DELETE_MSG("OPEX"), err.error.data.message || err.statusText)
          }
        })
      }
    })
  }

  onSaveChanges() {
    const isAllFromFilled = Object.keys(this.form).every(key => (this.form as any)[key])
    if (isAllFromFilled) {
      this.isFormInvalid = false
      this.isLoading = true
      if (!this.opexId) {
        this.apiService.insertOpex(this.form).subscribe({
          next: (res) => {
            this.searchSubject.next(this.searchTerm)
            this.modalService.dismissAll()
            this.isLoading = false
          },
          error: (err: HttpErrorResponse) => {
            this.common.showErrorAlert(Const.ERR_INSERT_MSG("OPEX"), err.error.data.message || err.statusText)
            this.isLoading = false
          }
        })
      } else {
        this.apiService.updateOpex(this.opexId, this.form).subscribe({
          next: (res) => {
            this.searchSubject.next(this.searchTerm)
            this.modalService.dismissAll()
            this.isLoading = false
          },
          error: (err: HttpErrorResponse) => {
            this.common.showErrorAlert(Const.ERR_UPDATE_MSG("OPEX"), err.error.data.message || err.statusText)
            this.isLoading = false
          }
        })
      }
    } else {
      this.isFormInvalid = true
    }
  }

  onSort(column: string) {
    if (this.sortColumn === column) {
      this.sortDirection = this.sortDirection === 'asc' ? 'desc' : 'asc';
    } else {
      this.sortColumn = column;
      this.sortDirection = 'asc';
    }
    this.searchSubject.next(this.searchTerm)
  }
}
