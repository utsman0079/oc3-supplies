import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OpexFixComponent } from './opex-fix.component';

describe('OpexFixComponent', () => {
  let component: OpexFixComponent;
  let fixture: ComponentFixture<OpexFixComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OpexFixComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OpexFixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
