import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualTransactionComponent } from './actual-transaction.component';

describe('ActualTransactionComponent', () => {
  let component: ActualTransactionComponent;
  let fixture: ComponentFixture<ActualTransactionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActualTransactionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ActualTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
