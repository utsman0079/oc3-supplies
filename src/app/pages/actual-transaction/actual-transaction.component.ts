import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { debounceTime, Subject } from 'rxjs';
import { CommonService } from 'src/app/core/services/common.service';
import { restApiService } from 'src/app/core/services/rest-api.service';
import { TokenStorageService } from 'src/app/core/services/token-storage.service';
import { Const } from 'src/app/core/static/const';
import { SortDirection } from 'src/app/global-component';

@Component({
  selector: 'app-actual-transaction',
  templateUrl: './actual-transaction.component.html',
  styleUrls: ['./actual-transaction.component.scss']
})
export class ActualTransactionComponent {

  lineData: any[] = []
  sectionData: any[] = []
  userData: any

  actualData: any[] = []
  lineId: number = -1
  costCtrId: number = -1

  isLoading = false;
  breadCrumbItems!: Array<{}>

  searchTerm = ''
  searchSubject = new Subject<string>()
  
  totalValue: number = 0

  totalItems = 0
  currentPage = 1
  pageSize = 10
  totalPages!: number
  sortColumn = 'entry_datetime'
  sortDirection: SortDirection = 'desc'

  dateRange = {
    from: '',
    to: ''
  }

  isSectionSelectDisabled = false
  isAllAccessGranted = false

  constructor(
    private apiService: restApiService, 
    private tokenService: TokenStorageService,
    public common: CommonService, 
    private router: Router, 
    private route: ActivatedRoute
  ) {
    this.userData = this.tokenService.getUser()
    this.breadCrumbItems = [
      { label: 'Actual Transactions History', active: true }
    ];
    this.searchSubject.pipe(debounceTime(350)).subscribe((term) => {
      this.searchActualTrByPagination(term)
    })
  }

  async ngOnInit() {
    this.lineData = await this.getFactoryLine()

    const query = this.route.snapshot.queryParams
    if (Object.keys(query).length > 0 && this.isAllAccessGranted) {
      if (query['lineId']) {
        this.lineId = +query['lineId']
        this.sectionData = await this.getSectionByLineId(this.lineId)
        if (query['costCtrId'])  this.costCtrId = +query['costCtrId']
      }
    }
    this.searchActualTrByPagination(this.searchTerm)
  }

  searchActualTrByPagination(term: string) {
    this.isLoading = true;
    this.apiService.getActualTransaction(
      term, this.currentPage, this.pageSize, this.sortColumn, this.sortDirection,
      this.dateRange.from, this.dateRange.to, this.lineId, this.costCtrId
    ).subscribe({
      next: (res: any) => {
        this.isLoading = false
        this.actualData = res.data
        this.totalItems = res.total
        this.totalValue = res.value
      },
      error: (err) => {
        this.isLoading = false
        this.common.showServerErrorAlert(Const.ERR_GET_MSG("Actual Transaction"), err)
      }
    })
  }

  async getFactoryLine() {
    return new Promise<any[]>((resolve, reject) => {
      this.isLoading = true
      this.apiService.getFactoryLine().subscribe({
        next: async (res) => {
          this.isLoading = false;
          let lineData: any[] = res.data
          const userLineAccess: any[] = JSON.parse(this.userData.line_access)
          if (userLineAccess.some(access => access.name === "All Access Granted")) {
            this.isAllAccessGranted = true
          } else {
            this.isAllAccessGranted = false
            lineData = [...lineData].filter(line => userLineAccess.some(access => access.id === line.id))
                .map(item => ({ id: item.id, name: item.name }))
            this.lineId = lineData[0].id
            this.sectionData = await this.getSectionByLineId(this.lineId)
          }
          resolve(lineData)
        },
        error: (err) => {
          this.isLoading = false
          this.common.showServerErrorAlert(Const.ERR_GET_MSG("Factory Line"), err)
          reject(err)
        }
      })
    })
  }

  async getSectionByLineId(lineId: number) {
    this.isLoading = true
    return new Promise<any[]>((resolve, reject) => {
      this.apiService.getCostCenterByLineId(lineId).subscribe({
        next: (res) => {
          this.isLoading = false
          resolve(res.data)
        },
        error: (err) => {
          this.isLoading = false
          this.common.showErrorAlert(Const.ERR_GET_MSG("Section"), err)
          reject(err)
        }
      })
    })
  }

  calculateStartingIndex(index: number): number {
    return (this.currentPage - 1) * this.pageSize + index + 1;
  }

  onSort(column: string) {
    if (this.sortColumn === column) {
      this.sortDirection = this.sortDirection === 'asc' ? 'desc' : 'asc';
    } else {
      this.sortColumn = column;
      this.sortDirection = 'asc';
    }
    this.searchSubject.next(this.searchTerm)
  }

  onChangeDateRange(event: any) {
    const selectedDates: any[] = event.selectedDates
    if (selectedDates.length === 2) {
      const date: string = event.dateString
      if (date.includes("to")) {
        this.dateRange = { from: date.split(" to ")[0], to: date.split(" to ")[1] }
      } else {
        this.dateRange = { from: date, to: date }
      }
      this.searchSubject.next(this.searchTerm)
    }
  }

  async onFactoryLineFilter() {
    this.costCtrId = -1
    this.sectionData = this.lineId !== -1 ? await this.getSectionByLineId(this.lineId) : []
    this.searchActualTrByPagination(this.searchTerm)
    this.router.navigate([], { queryParams: { lineId: this.lineId } })
  }

  onSectionFilter() {
    this.router.navigate([], { queryParams: { costCtrId: this.costCtrId }, queryParamsHandling: 'merge' })
    this.searchActualTrByPagination(this.searchTerm)
  }
}
