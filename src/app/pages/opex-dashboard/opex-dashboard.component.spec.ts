import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OpexDashboardComponent } from './opex-dashboard.component';

describe('OpexDashboardComponent', () => {
  let component: OpexDashboardComponent;
  let fixture: ComponentFixture<OpexDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OpexDashboardComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OpexDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
