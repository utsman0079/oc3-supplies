import { Component, ElementRef, NgZone, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, debounceTime } from 'rxjs';
import { CommonService } from 'src/app/core/services/common.service';
import { EventService } from 'src/app/core/services/event.service';
import { restApiService } from 'src/app/core/services/rest-api.service';
import { TokenStorageService } from 'src/app/core/services/token-storage.service';
import { ChartType } from '../dashboard/dashboard.model';
import { Const } from 'src/app/core/static/const';

interface DashboardChart {
  rawData?: any[];
  series?: any[];
  categories?: any[];
}

@Component({
  selector: 'app-opex-dashboard',
  templateUrl: './opex-dashboard.component.html',
  styleUrls: ['./opex-dashboard.component.scss']
})
export class OpexDashboardComponent {
  emptyChartData = () => { return { rawData: [], series: [], categories: [] } as DashboardChart }

  opexPerLineColumnChart!: ChartType;
  opexPerMonthColumnChart!: ChartType;
  opexTotalPerSectionDonutChart!: ChartType;
  opexPerSectionBarChart!: ChartType;
  opexSectionPerMonthColumnChart!: ChartType;
  opexSectionPerItemTreemapChart!: ChartType;

  opexPerLineData: any[] = []
  opexPerLineLastYearData: any[] = []
  opexPerSectionMonthData: any[] = []
  opexSectionPerItemData: any[] = []
  opexTop5BiggestItemData: any[] = []
  opexPerSectionData: any[] = []
  opexPerMonthData: any[] = []

  yearSubject = new Subject<number>()
  isLoading = false;

  lineData: any[] = []
  selectedLine = {id: 0, name: ''}

  sectionData: any[] = []
  selectedSection = {id: 0, name: ''}

  year: number
  costCtrId: number = 0

  totalFactoryBudget: number = 0;
  totalFactoryActual: number = 0;

  months: any[] = []
  selectedMonth = {
    number: -1,
    name: 'All Month'
  }

  userData: any
  isBudgetNotEmpty = false

  tabData = [{id: 1, name: 'Budget'}, {id: 2, name: 'Actual'}]
  isTabOpen = { budget: true, actual: false }
  activeTab = 1

  activeDonutChartDropdown: 'Budget' | 'Actual' = 'Budget'
  isCompareLastYearOpex = false;
  isAllAccessGranted = false;

  showTreemapLabels = true

  @ViewChild("line") line!: ElementRef
  @ViewChild("section") section!: ElementRef

  constructor(
    public common: CommonService,
    private apiService: restApiService,
    private router: Router,
    private route: ActivatedRoute,
    private tokenService: TokenStorageService,
    private eventService: EventService
  ) {
    this.userData = this.tokenService.getUser()

    const queryParams = this.route.snapshot.queryParams
    if (queryParams['year']) {
      this.year = queryParams['year']
    } else {
      this.year = new Date().getFullYear()
    }

    this.yearSubject.pipe(debounceTime(350)).subscribe(year => this.ngOnInit())
    this.eventService.subscribe('changeMode', (mode) => {
      const color = mode == 'dark' ? "#fff" : "#000";
      const charts: ChartType[] = [
        this.opexPerLineColumnChart,
        this.opexPerMonthColumnChart,
        this.opexTotalPerSectionDonutChart,
        this.opexPerSectionBarChart,
        this.opexSectionPerMonthColumnChart
      ]
      charts.forEach(chart => this.changeChartLabelColor(chart, color))
    })
  }

  async ngOnInit() {
    this.months = [{ number: -1, name: 'All Month'}]
    this.isCompareLastYearOpex = false;

    this.opexPerLineData = await this.getOpexPerLine(this.year)
    const userLineAccess: any[] = JSON.parse(this.userData.line_access)
    const lines = this.opexPerLineData.map(opex => ({ id: opex.line_id, name: opex.line }))
    if (userLineAccess.some(item => item.name === "All Access Granted")) {
      this.isAllAccessGranted = true;
      this.lineData = lines
    } else {
      this.isAllAccessGranted = false;
      this.lineData = [...lines].filter(item => userLineAccess.some(line => line.id === item.id))
        .map(item => ({ id: item.id, name: item.name }))
    }
    this.selectedLine = { id: this.lineData[0].id, name: this.lineData[0].name  }

    this.opexPerSectionMonthData = await this.getOpexPerSectionMonthByLine(this.year, this.selectedLine.id)
    this.getLineSections()
    this.opexPerSectionData = this.getOpexPerSections(this.opexPerSectionMonthData)
    this.opexPerMonthData = this.getOpexPerMonths(this.opexPerSectionMonthData)

    this.opexSectionPerItemData = await this.getOpexPerItemBySection(this.year, this.selectedSection.id)
    this.opexTop5BiggestItemData = [...this.opexSectionPerItemData].sort((a, b) => b.budget - a.budget).slice(0, 5)
      .map(item => ({ order: item.order, opex: item.opex, price: item.budget}))

    this._opexPerLineColumnChart('["--vz-primary", "--vz-info", "--vz-secondary", "--vz-success"]')
    this._opexPerMonthColumnChart('["--vz-primary", "--vz-info", "--vz-success", "--vz-secondary"]')
    this._opexTotalPerSectionDonutChart('["--vz-success", "--vz-primary", "--vz-info", "--vz-success-rgb, 0.60", "--vz-primary-rgb, 0.45", "--vz-info-rgb, 0.30"]')
    this._opexPerSectionBarChart('["--vz-primary", "--vz-info", "--vz-success", "--vz-secondary"]')
    this._opexSectionPerMonthColumnChart('["--vz-primary", "--vz-info", "--vz-success", "--vz-secondary"]')
    this._opexSectionPerItemTreemapChart('["--vz-primary"]')
  }

  async getOpexPerLine(year: number) {
    return new Promise<any[]>((resolve, reject) => {
      this.isLoading = true;
      this.apiService.getOpexPerLineByYear(year).subscribe({
        next: (res) => {
          let data: any[] = res.data
          if (data.length > 0) {
            this.totalFactoryBudget = this.common.sumElementFromArray(data, 'budget')
            this.totalFactoryActual = this.common.sumElementFromArray(data, 'actual')
            this.isBudgetNotEmpty = true;

            const lastYear = year - 1;
            this.apiService.getOpexPerLineByYear(lastYear).subscribe({
              next: (_res) => {
                this.isLoading = false;
                const lastYearData = data.map(item => ({ line_id: +item.line_id, line: `${item.line}`, budget: 0, actual: 0 }))
                lastYearData.forEach(item => {
                  const index = (_res.data as any[]).findIndex(obj => obj.line_id === item.line_id)
                  if (index !== -1) {
                    item.budget = _res.data[index].budget
                    item.actual = _res.data[index].actual
                  }
                })
                this.opexPerLineLastYearData = lastYearData
                resolve(data)
              },
              error: (_err) => {
                this.isLoading = false;
                this.common.showServerErrorAlert(Const.ERR_GET_MSG("Opex per Line Last Year"), _err)
                reject(_err)
              }
            })
            
          } else {
            this.isLoading = false;
            this.isBudgetNotEmpty = false;
          }
        },
        error: (err) => {
          this.isLoading = false;
          this.common.showServerErrorAlert(Const.ERR_GET_MSG("Opex per Line"), err) 
          reject(err)
        }
      })
    })
  }

  async getOpexPerSectionMonthByLine(year: number, lineId: number) {
    return new Promise<any[]>((resolve, reject) => {
      this.isLoading = true;
      this.apiService.getOpexPerSectionMonthByLine(year, lineId).subscribe({
        next: (res) => {
          this.isLoading = false;
          let data: any[] = res.data
          resolve(data)
        },
        error: (err) => {
          this.isLoading = false;
          this.common.showServerErrorAlert(Const.ERR_GET_MSG("Opex per Section"), err) 
          reject(err)
        }
      })
    })
  }

  async getOpexPerItemBySection(year: number, costCtrId: number) {
    return new Promise<any[]>((resolve, reject) => {
      this.isLoading = true;
      this.apiService.getOpexPerItemByYearAndCostCtr(year, costCtrId).subscribe({
        next: (res) => {
          this.isLoading = false;
          let data: any[] = res.data
          resolve(data)
        },
        error: (err) => {
          this.isLoading = false;
          this.common.showServerErrorAlert(Const.ERR_GET_MSG("Opex per Item"), err) 
          reject(err)
        }
      })
    })
  }

  getLineSections() {
    const sections = new Set<number>()
    this.sectionData.splice(0)
    this.opexPerSectionMonthData.forEach(opex => {
      if (!sections.has(opex.cost_ctr_id)) {
        this.sectionData.push({id: opex.cost_ctr_id, name: opex.section})
      }
      sections.add(opex.cost_ctr_id)
    })
    this.selectedSection = { id: this.sectionData[0].id, name: this.sectionData[0].name }
  }

  getOpexPerSections(arr: any[]): any[] {
    const result = Object.values(arr.reduce((acc: any, { cost_ctr_id, section, actual, budget }) => {
      if (!acc[section]) {
          acc[section] = { cost_ctr_id, section, actual: 0, budget: 0 };
      }
      acc[section].actual += actual;
      acc[section].budget += budget;
      return acc;
    }, {}))

    return result
  }

  getOpexPerMonths(arr: any[]): any[] {
    const result = Object.values(arr.reduce((acc, { month, actual, budget }) => {
      if (!acc[month]) {
          acc[month] = { month, actual: 0, budget: 0 };
      }
      acc[month].actual += actual;
      acc[month].budget += budget;
      return acc;
    }, {}));
    
    return result
  }

  async onFactoryLineChange(data: any) {
    const isActual = this.activeDonutChartDropdown === 'Actual' ? true : false
    this.selectedLine = { id: data.id, name: data.name }
    this.opexPerSectionMonthData = await this.getOpexPerSectionMonthByLine(this.year, this.selectedLine.id)
    this.getLineSections()
    this.opexPerSectionData = this.getOpexPerSections(this.opexPerSectionMonthData)
    this.opexPerMonthData = this.getOpexPerMonths(this.opexPerSectionMonthData)
    
    this.setOpexPerMonthChartValue()
    this.setOpexTotalPerSectionChartValue(isActual)
    this.setOpexPerSectionChartValue()

    this.onSectionChange(this.selectedSection)
  }

  async onSectionChange(sectionData: any) {
    const price = this.isTabOpen.actual ? 'actual' : 'budget'
    this.selectedSection = { id: sectionData.id, name: sectionData.name }
    this.opexSectionPerItemData = await this.getOpexPerItemBySection(this.year, this.selectedSection.id)
    this.opexTop5BiggestItemData = [...this.opexSectionPerItemData].sort((a, b) => b[price] - a[price]).slice(0, 5)
      .map(item => ({ order: item.order, opex: item.opex, price: item[price]}))
    this.setOpexSectionPerMonthChartValue()
    this.setOpexSectionPerItemChartValue(this.isTabOpen.actual)
  }

  onDashboardTypeChange(event: any) {
    if (event.target.value == "Supplies") {
      this.router.navigate([`./`], { queryParams: { year: this.year } })
    } 
  }

  onBudgetActualTabChange(event: any) {
    const tab = JSON.parse(event.target.name)

    if (!this.isTabOpen.actual && tab.name === 'Actual') {
      this.opexTop5BiggestItemData = [...this.opexSectionPerItemData].sort((a, b) => b.actual - a.actual).slice(0, 5)
        .map(item => ({ order: item.order, opex: item.opex, price: item.actual}))
      
      this.setOpexSectionPerItemChartValue(true)

      this.isTabOpen = {actual: true, budget: false}
      this.activeTab = 2

    } else if (!this.isTabOpen.budget && tab.name === 'Budget') {
      this.opexTop5BiggestItemData = [...this.opexSectionPerItemData].sort((a, b) => b.budget - a.budget).slice(0, 5)
        .map(item => ({ order: item.order, opex: item.opex, price: item.budget}))

      this.setOpexSectionPerItemChartValue(false)

      this.isTabOpen = {actual: false, budget: true}
      this.activeTab = 1

    }

  }

  setOpexSectionPerItemChartValue(isActual = false) {
    const price = isActual ? 'actual' : 'budget'
    this.opexSectionPerItemTreemapChart.series = [
      { data: [...this.opexSectionPerItemData].sort((a, b) => b[price] - a[price]).map(item => ({x: item.opex, y: item[price]})) }
    ]
  }

  setOpexTotalPerSectionChartValue(isActual = false) {
    const price = isActual ? 'actual' : 'budget'
    this.opexTotalPerSectionDonutChart.series = this.opexPerSectionData.map(item => item[price])
    this.opexTotalPerSectionDonutChart.labels = this.opexPerSectionData.map(item => item.section)
  }

  setOpexSectionPerMonthChartValue() {
    const filteredSectionOpex = this.opexPerSectionMonthData.filter(item => item.cost_ctr_id === this.selectedSection.id)
    this.opexSectionPerMonthColumnChart.series = [
      {
        name: "Budget",
        data: filteredSectionOpex.map(item => item.budget),
      },
      {
        name: "Actual",
        data: filteredSectionOpex.map(item => item.actual),
      },
    ]
  }

  setOpexPerSectionChartValue() {
    this.opexPerSectionBarChart.series = [
      {
        name: 'Budget',
        data: this.opexPerSectionData.map(item => item.budget)
      },
      {
        name: 'Actual',
        data: this.opexPerSectionData.map(item => item.actual)
      },
    ]
    this.opexPerSectionBarChart.xaxis = {
      categories: this.opexPerSectionData.map(item => item.section),
      labels: {
        show: true,
        formatter: (val: any) => this.common.formatRupiahBigNumber(+val)
      }
    }
  }

  setOpexPerMonthChartValue() {
    this.opexPerMonthColumnChart.series = [
      {
        name: "Budget",
        data: this.opexPerMonthData.map(item => item.budget),
      },
      {
        name: "Actual",
        data: this.opexPerMonthData.map(item => item.actual),
      },
    ]
    this.opexPerMonthColumnChart.xaxis = {
      categories: this.opexPerMonthData.map(item => this.common.getSimpleMonthName(item.month)),
    }
  }

  onBudgetActualDropdownChange(mode: 'Budget' | 'Actual') {
    this.activeDonutChartDropdown = mode
    this.setOpexTotalPerSectionChartValue(mode === 'Actual' ? true : false)
  }

  onTreemapShowLabels(event: any) {
    this.showTreemapLabels = event.target.checked
    setTimeout(() => {
      this.opexSectionPerItemTreemapChart.dataLabels = { enabled: event.target.checked }
    }, 50)
  }

  onCheckedLastYearComparison(event: any) {
    this.isCompareLastYearOpex = event.target.checked
    const series = (data: any[], isLastYear = false) => {
      const lastYear = this.year - 1
      const result = [
        {
          name: `Budget${isLastYear ? ' (' + lastYear + ')' : ''}`,
          data: data.map(item => item.budget),
        },
        {
          name: `Actual${isLastYear ? ' (' + lastYear + ')' : ''}`,
          data: data.map(item => item.actual),
        }
      ]
      return result
    }
    setTimeout(() => {
      if (event.target.checked) {
        this.opexPerLineColumnChart.series = [
          ...series(this.opexPerLineData), 
          ...series(this.opexPerLineLastYearData, true)
        ]
      } else {
        this.opexPerLineColumnChart.series = series(this.opexPerLineData)
      }
    }, 50)
  }

  async changeFactoryLine(lineId: number) {

  }

  changeChartLabelColor(chart: ChartType, color: string) {
    chart.dataLabels = {
      ...chart.dataLabels,
      style: {
        ...chart.dataLabels.style,
        colors: [color],
      },
    }
  }

  onButtonChangeYear(action: string) {
    switch (action) {
      case "prev":
        this.year--;
        break;
      case "next":
        this.year++;
        break;
      default:
        break;
    }
    this.router.navigate([], { queryParams: { year: this.year }, queryParamsHandling: 'merge' })
    this.yearSubject.next(this.year)
  }

  onYearChange(event: any) {
    if (event.target.value) {
      this.router.navigate([], { queryParams: { year: this.year }, queryParamsHandling: 'merge' })
      this.yearSubject.next(this.year)
    }
  }

  private getChartColorsArray(colors: any) {
    colors = JSON.parse(colors);
    return colors.map(function (value: any) {
      var newValue = value.replace(" ", "");
      if (newValue.indexOf(",") === -1) {
        var color = getComputedStyle(document.documentElement).getPropertyValue(newValue);
        if (color) {
          color = color.replace(" ", "");
          return color;
        }
        else return newValue;;
      } else {
        var val = value.split(',');
        if (val.length == 2) {
          var rgbaColor = getComputedStyle(document.documentElement).getPropertyValue(val[0]);
          rgbaColor = "rgba(" + rgbaColor + "," + val[1] + ")";
          return rgbaColor;
        } else {
          return newValue;
        }
      }
    });
  }

  private _opexPerLineColumnChart(colors: any) {
    colors = this.getChartColorsArray(colors);
    this.opexPerLineColumnChart = {
      series: [{
        name: "Budget",
        data: this.opexPerLineData.map(item => item.budget),
      },
      {
        name: "Actual",
        data: this.opexPerLineData.map(item => item.actual),
      }
      ],
      chart: {
        width: '100%',
        height: 350,
        type: "bar",
        toolbar: {
          show: false,
        },
        events: {
          click: (event: any, context: any, config: any) => {
            const index = config.dataPointIndex
            if (this.opexPerLineData.length > 0 && index !== -1) {
              const lineData = {
                id: this.opexPerLineData[index].line_id,
                name: this.opexPerLineData[index].line
              }
              this.onFactoryLineChange(lineData)
              this.line.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' })
              setTimeout(() => {
                const currentPos = window.scrollY || window.pageYOffset
                this.isLoading = false
                window.scrollTo({top: currentPos - 65, behavior: 'auto'})
              }, 750)
            }
            
          }
        },
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: "75%",
          dataLabels: {
            position: 'top'
          }
        },
      },
      dataLabels: {
        enabled: true,
        textAnchor: 'middle',
        offsetY: -30,
        style: {
          fontSize: "10px",
          colors: [this.getChartLabelColor()],
        },
        formatter: (val: any, { seriesIndex, dataPointIndex, w }: any) => {
          if (seriesIndex === 1) {
            const data = this.opexPerLineData.map(item => item.budget)
            const budget: number = data[dataPointIndex]
            return [
              `${this.common.formatRupiahBigNumber(+val)} `, 
              `(${( (this.setProdplanPercentage(budget, +val) === 0 ? 0 : 100) + this.setProdplanPercentage(budget, +val)).toFixed(1)}%)`
            ]
          }
          return this.common.formatRupiahBigNumber(+val)
        }
      },
      stroke: {
        show: true,
        width: 2,
        colors: ["transparent"],
      },
      colors: colors,
      xaxis: {
        categories: this.opexPerLineData.map(item => item.line),
        labels: {
          style: {
            fontWeight: "bold",
          }
        }
      },
      yaxis: {
        title: {
          text: "Rp (Rupiah)",
        },
        labels: {
          show: true,
          formatter: (val: any) => this.common.formatRupiahBigNumber(+val)
        }
      },
      grid: {
        borderColor: "#f1f1f1",
      },
      fill: {
        opacity: 1,
      },
      tooltip: {
        y: {
          formatter: (val: any) => this.common.getRupiahFormat(+val)
        },
      },
    };
  }

  private _opexPerMonthColumnChart(colors: any) {
    colors = this.getChartColorsArray(colors);
    this.opexPerMonthColumnChart = {
      series: [{
        name: "Budget",
        data: this.opexPerMonthData.map(item => item.budget),
      },
      {
        name: "Actual",
        data: this.opexPerMonthData.map(item => item.actual),
      },
      ],
      chart: {
        height: 350,
        type: "bar",
        toolbar: {
          show: false,
        },
        events: {
          click: (event: any, context: any, config: any) => {
            const index = config.dataPointIndex
            if (index !== -1) {
              //
            }
          }
        }
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: "75%",
          dataLabels: {
            position: 'top'
          }
        },
      },
      dataLabels: {
        enabled: true,
        textAnchor: 'middle',
        offsetY: -20,
        style: {
          fontWeight: 400,
          fontSize: "10px",
          colors: [this.getChartLabelColor()],
        },
        formatter: (val: any) => this.common.formatRupiahBigNumber(+val)
      },
      stroke: {
        show: true,
        width: 2,
        colors: ["transparent"],
      },
      colors: colors,
      xaxis: {
        categories: this.opexPerMonthData.map(item => this.common.getSimpleMonthName(item.month)),
      },
      yaxis: {
        title: {
          text: "Rp (Rupiah)",
        },
        labels: {
          show: true,
          formatter: (val: any) => this.common.formatRupiahBigNumber(+val)
        }
      },
      grid: {
        borderColor: "#f1f1f1",
      },
      fill: {
        opacity: 1,
      },
      tooltip: {
        x: {
          formatter: (val: any, opt: any) =>  `${this.common.getMonthName(this.opexPerMonthData[opt.dataPointIndex].month)}`
        },
        y: {
          formatter: (val: any) => this.common.getRupiahFormat(+val)
        },
      },
    };
  }

  private _opexTotalPerSectionDonutChart(colors: any) {
    colors = this.getChartColorsArray(colors);
    this.opexTotalPerSectionDonutChart = {
      series: this.opexPerSectionData.map(item => item.budget),
      labels: this.opexPerSectionData.map(item => item.section),
      chart: {
        type: "donut",
        height: 250,
      },
      plotOptions: {
        pie: {
          offsetX: 0,
          offsetY: 0,
          donut: {
            size: "85%",
            labels: {
              show: true,
              name: {
                show: true,
                fontSize: '16px',
                offsetY: -5,
              },
              value: {
                show: true,
                fontSize: '16px',
                color: '#343a40',
                fontWeight: 500,
                offsetY: 5,
                formatter: (val: any) => this.common.getRupiahFormat(+val)
              },
              total: {
                show: true,
                fontSize: '12px',
                label: 'Total value',
                color: '#9599ad',
                fontWeight: 500,
                formatter: (val: any) => this.common.getRupiahFormat(this.common.sumElementFromArray(val.globals.series))
              }
            }
          },
        },
      },
      dataLabels: {
        enabled: false,
      },
      legend: {
        show: false,
      },
      yaxis: {
        labels: {
          formatter: (value: any) => this.common.getRupiahFormat(+value)
        }
      },
      stroke: {
        lineCap: "round",
        width: 2
      },
      colors: colors
    };
  }

  private _opexPerSectionBarChart(colors: any) {
    colors = this.getChartColorsArray(colors);
    this.opexPerSectionBarChart = {
      series: [
        {
          name: 'Budget',
          data: this.opexPerSectionData.map(item => item.budget)
        },
        {
          name: 'Actual',
          data: this.opexPerSectionData.map(item => item.actual)
        },
      ],
      chart: {
        type: "bar",
        height: 410,
        toolbar: {
          show: false,
        },
        events: {
          click: (event: any, context: any, config: any) => {
            if (config.dataPointIndex !== -1) {
              const sectionData = {
                id: this.opexPerSectionData[config.dataPointIndex].cost_ctr_id,
                name: this.opexPerSectionData[config.dataPointIndex].section
              }
              
              this.onSectionChange(sectionData)
              this.section.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
              setTimeout(() => {
                const currentPos = window.scrollY || window.pageYOffset
                this.isLoading = false
                window.scrollTo({top: currentPos - 65, behavior: 'auto'})
              }, 500)
            }
          }
        },
      },
      plotOptions: {
        bar: {
          horizontal: true,
          dataLabels: {
            position: "top",
          },
        },
      },
      dataLabels: {
        enabled: true,
        offsetX: -6,
        style: {
          fontSize: "10px",
          fontWeight: 400,
          colors: [this.getChartLabelColor()],
        },
        formatter: (val: any) => this.common.formatRupiahBigNumber(+val)
      },
      stroke: {
        show: true,
        width: 1,
        colors: ["#fff"],
      },
      tooltip: {
        shared: true,
        intersect: false,
        y: {
          formatter: (val: any) => this.common.getRupiahFormat(+val)
        },
      },
      xaxis: {
        categories: this.opexPerSectionData.map(item => item.section),
        labels: {
          show: true,
          formatter: (val: any) => this.common.formatRupiahBigNumber(+val)
        }
      },
      yaxis: {
        labels: {
          style: {
            fontWeight: "bold",
          }
        }
      },
      colors: colors,
    };
  }

  private _opexSectionPerMonthColumnChart(colors: any) {
    colors = this.getChartColorsArray(colors);
    const filteredSectionOpex = this.opexPerSectionMonthData.filter(item => item.cost_ctr_id === this.selectedSection.id)
    this.opexSectionPerMonthColumnChart = {
      series: [{
        name: "Budget",
        data: filteredSectionOpex.map(item => item.budget),
      },
      {
        name: "Actual",
        data: filteredSectionOpex.map(item => item.actual),
      },
      ],
      chart: {
        height: 350,
        type: "bar",
        toolbar: {
          show: false,
        },
        events: {
          click: (event: any, context: any, config: any) => {
            const index = config.dataPointIndex
            if (index !== -1) {
              //
            }
          }
        }
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: "75%",
          dataLabels: {
            position: 'top'
          }
        },
      },
      dataLabels: {
        enabled: true,
        textAnchor: 'middle',
        offsetY: -20,
        style: {
          fontWeight: 400,
          fontSize: "10px",
          colors: [this.getChartLabelColor()],
        },
        formatter: (val: any) => this.common.formatRupiahBigNumber(+val)
      },
      stroke: {
        show: true,
        width: 2,
        colors: ["transparent"],
      },
      colors: colors,
      xaxis: {
        categories: filteredSectionOpex.map(item => this.common.getSimpleMonthName(item.month)),
      },
      yaxis: {
        title: {
          text: "Rp (Rupiah)",
        },
        labels: {
          show: true,
          formatter: (val: any) => this.common.formatRupiahBigNumber(+val)
        }
      },
      grid: {
        borderColor: "#f1f1f1",
      },
      fill: {
        opacity: 1,
      },
      tooltip: {
        x: {
          formatter: (val: any, opt: any) =>  `${this.common.getMonthName(filteredSectionOpex[opt.dataPointIndex].month)}`
        },
        y: {
          formatter: (val: any) => this.common.getRupiahFormat(+val)
        },
      },
    };
  }

  private _opexSectionPerItemTreemapChart(colors: any) {
    colors = this.getChartColorsArray(colors);
    const chartSeries = [...this.opexSectionPerItemData].sort((a, b) => b.budget - a.budget).map(item => ({x: item.opex, y: item.budget}))
    this.opexSectionPerItemTreemapChart = {
      series: [{
        data: chartSeries
      },],
      legend: {
        show: false,
      },
      chart: {
        height: 350,
        type: "treemap",
        toolbar: {
          show: false,
        },
      },
      colors: colors,
      dataLabels: {
        enabled: true
      },
      tooltip: {
        x: {
          show: true,
        },
        y: {
          formatter: (val: any, opt: any) => this.common.getRupiahFormat(+val)
        },
      },
    };
  }

  setProdplanPercentage(plan: number, actual: number) {
    const difference = actual - plan
    const percentage = (difference / plan) * 100;
    return (plan && actual) == 0 ? 0 : percentage
  }

  getChartLabelColor(): string {
    const theme = localStorage.getItem('theme');
    if (theme === 'dark') {
      return '#ffffff';
    } else {
      return '#000000';
    }
  }
}
