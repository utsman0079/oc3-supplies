import { DatePipe } from '@angular/common';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from 'src/app/core/services/common.service';
import { restApiService } from 'src/app/core/services/rest-api.service';
import { TokenStorageService } from 'src/app/core/services/token-storage.service';
import { Const } from 'src/app/core/static/const';

interface AveragePrice { 
  avg_price_id?: number,
  material_id?: number, 
  year?: number, 
  average_price?: number,
  updated_at?: string,
  is_removed?: number 
}

@Component({
  selector: 'app-detail-supply',
  templateUrl: './detail-supply.component.html',
  styleUrls: ['./detail-supply.component.scss']
})
export class DetailSupplyComponent {
  materialCode!: number
  materialData: any;
  actualTransactionData: any[] = []
  mergeBudgetActualData: any
  detailPrice: AveragePrice[] = []

  isLoading = false;
  userData: any
  uomData: string[] = []

  dateRange = {
    from: new Date(),
    to: new Date()
  }
  datePlaceholder = ''

  breadCrumbItems!: Array<{}>;
  budgetId = ''
  budgetData = {
    year: '',
    lineId: '',
    costCtrId: '',
    materialId: ''
  }

  selectedUsageSection: string = ''
  usageSectionData: any[] = []

  lineData: any[] = []
  costCenterData: any[] = []

  isAllLineSelected = false
  isAllAccessGranted = false

  modalKeys: any[] = []
  modalData: any

  constructor(
    private route: ActivatedRoute, 
    private apiService: restApiService, 
    public common: CommonService,
    private tokenService: TokenStorageService,
    private datePipe: DatePipe,
    private modalService: NgbModal,
    ) {
    this.breadCrumbItems = [
      { label: 'Supplies', active: false },
      { label: 'Detail', active: true }
    ];
    this.userData = this.tokenService.getUser()
  }

  async ngOnInit() {
    this.lineData = await this.getFactoryLine()
    this.usageSectionData = await this.getAllCostCenterWithLineId()

    const userLineAccess: any[] = JSON.parse(this.userData.line_access)
    if (userLineAccess.some(access => access.name === "All Access Granted")) {
      this.isAllAccessGranted = true
    } else {
      this.isAllAccessGranted = false
      this.lineData = this.lineData.filter(line => userLineAccess.some(access => access.id === line.id))
      this.usageSectionData = this.usageSectionData.filter(line => userLineAccess.some(access => access.id === line.line_id))
    }

    window.scrollTo({ top: 0, behavior: 'auto' });

    this.route.params.subscribe( async params => {
      this.uomData = await this.getMaterialUOM()
      this.materialCode = +params['code']
      this.materialData = await this.getDetailMaterial(+params['code'])
      this.detailPrice = this.materialData.detail_price.sort((a: any, b: any) => b.year - a.year)

      this.budgetId = this.route.snapshot.queryParams['budgetId']
      if (this.budgetId) {
        [this.budgetData.year, this.budgetData.lineId, this.budgetData.costCtrId, this.budgetData.materialId] = this.budgetId.split('-')
        this.selectedUsageSection = JSON.stringify({cost_ctr_id: +this.budgetId.split('-')[2], line_id: +this.budgetId.split('-')[1]})

        this.costCenterData = await this.getCostCenterByLineId(+this.budgetData.lineId)
      
        this.mergeBudgetActualData = await this.getMergeBudgetActualDetail(this.budgetId)
      } else {
        if (this.isAllAccessGranted) {
          this.budgetData.lineId = '-1'
          this.budgetData.costCtrId = '-1'
          this.isAllLineSelected = true
        } else {
          this.budgetData.lineId = `${this.lineData[0].id}`
          this.costCenterData = await this.getCostCenterByLineId(+this.budgetData.lineId)
          this.budgetData.costCtrId = `${this.costCenterData[0].id}`
        }
      }

      this.actualTransactionData = await this.getActualTransactionDetail(this.materialCode, +this.budgetData.lineId, +this.budgetData.costCtrId)
      if (this.actualTransactionData.length > 0) {
        this.dateRange = {
          from: new Date(this.actualTransactionData[this.actualTransactionData.length - 1].entry_datetime),
          to: new Date(this.actualTransactionData[0].entry_datetime)
         }
      }
      
    })
  }

  async getDetailMaterial(materialCode: number) {
    return new Promise<any>((resolve, reject) => {
      this.isLoading = true;
      this.apiService.getMaterialByCode(materialCode).subscribe({
        next: (res: any) => {
          this.isLoading = false;
          resolve(res.data[0])
        },
        error: (err) => {
          this.isLoading = false;
          this.common.showServerErrorAlert(Const.ERR_GET_MSG("Material"), err)
          reject(err)
        }
      })
    })
  }

  async getMaterialUOM() {
    return new Promise<any[]>((resolve, reject) => {
      this.isLoading = true
      this.apiService.getMaterialUOM().subscribe({
        next: (res) => {
          this.isLoading = false
          resolve(res.data)
        },
        error: (err) => {
          this.isLoading = false
          this.common.showServerErrorAlert(Const.ERR_GET_MSG("Material UOM"), err)
          reject(err)
        }
      })
    })
  }

  async getActualTransactionDetail(materialCode: number, lineId: number, costCtrId: number, fromDate = '', toDate = '') {
    return new Promise<any[]>((resolve, reject) => {
      this.isLoading = true
      this.apiService.getActualTransactionDetail(materialCode, lineId, costCtrId, fromDate, toDate).subscribe({
        next: async (res) => {
          this.isLoading = false;
          let data: any[] = res.data
          data.sort((a, b) => new Date(b.entry_datetime).getTime() - new Date(a.entry_datetime).getTime())
          resolve(data)
        },
        error: (err) => {
          this.isLoading = false
          this.common.showServerErrorAlert(Const.ERR_GET_MSG("Actual Transaction Detail"), err)
          reject(err)
        }
      })
    })
  }

  async getMergeBudgetActualDetail(budgetId: string) {
    return new Promise<any>((resolve, reject) => {
      this.isLoading = true
      this.apiService.getMergeBudgetActualDetail(budgetId).subscribe({
        next: (res) => {
          this.isLoading = false;
          resolve(res.data[0])
        },
        error: (err) => {
          this.isLoading = false
          this.common.showServerErrorAlert(Const.ERR_GET_MSG("Merge Budget Actual"), err)
          reject(err)
        }
      })
    })
  }

  async getFactoryLine() {
    return new Promise<any[]>((resolve, reject) => {
      this.isLoading = true
      this.apiService.getFactoryLine().subscribe({
        next: (res) => {
          this.isLoading = false;
          resolve(res.data)
        },
        error: (err) => {
          this.isLoading = false
          this.common.showServerErrorAlert(Const.ERR_GET_MSG("Factory Line"), err)
          reject(err)
        }
      })
    })
  }

  async getCostCenterByLineId(lineId: number) {
    return new Promise<any[]>((resolve, reject) => {
      this.isLoading = true
      this.apiService.getCostCenterByLineId(lineId).subscribe({
        next: (res) => {
          this.isLoading = false;
          resolve(res.data)
        },
        error: (err) => {
          this.isLoading = false
          this.common.showServerErrorAlert(Const.ERR_GET_MSG("Cost Center"), err)
          reject(err)
        }
      })
    })
  }

  async getAllCostCenterWithLineId() {
    return new Promise<any[]>((resolve, reject) => {
      this.isLoading = true
      this.apiService.getAllCostCenteryWithLineId().subscribe({
        next: (res) => {
          const data: any[] = res.data
          const lines = this.common.getUniqueData(data, "line_id").map(line => ({ line_id: line.line_id, line: line.line } as any))
          lines.forEach(line => {
            let sections: any[] = []
            data.forEach(item => {
              if (item.line_id === line.line_id) {
                sections.push({ id: item.id, section: item.section, cost_ctr: item.cost_ctr })
              }
            })
            line.sections = sections
          })
          resolve(lines.sort((a, b) => a.line_id - b.line_id))
        },
        error: (err) => {
          this.isLoading = false
          this.common.showServerErrorAlert(Const.ERR_GET_MSG("Cost Center with Line"), err)
          reject(err)
        }
      })
    })
  }

  getLatestAveragePrice(detailPrice: any[]): number {
    if (!detailPrice || detailPrice.length < 1) {
      return 0
    }
    const years = detailPrice.map((item) => item.year)
    const latestYear = Math.max(...years)
    return detailPrice.filter((item) => item.year === latestYear)[0].average_price || 0
  }

  setAveragePricePercentage(avgBefore: number, avgNow: number) {
    if (avgBefore == 0 && avgNow == 0) return 0
    const difference = avgNow - avgBefore
    const percentage = (difference / avgBefore) * 100;
    return avgBefore == 0 ? 0 : percentage
  }

  async onChangeDateRange(event: any) {
    const value = event.target.value as string
    const datesArray = value.split(' to ');
    if (datesArray.length === 2) {
      const [fromDate, toDate] = datesArray
      this.actualTransactionData = await this.getActualTransactionDetail(this.materialCode, +this.budgetData.lineId, +this.budgetData.costCtrId, fromDate, toDate)
      this.dateRange = {
        from: new Date(fromDate),
        to: new Date(toDate)
      }
    }
  }

  async onFactoryLineChange(event: any) {
    const lineId = +event.target.value
    lineId !== -1 ? this.isAllLineSelected = false : this.isAllLineSelected = true
    this.budgetData.lineId = `${lineId}`

    const transformDate = (date: Date) => this.datePipe.transform(date, 'yyyy-MM-dd')
    const [fromDate, toDate] = [transformDate(this.dateRange.from), transformDate(this.dateRange.to)]
    this.budgetData.costCtrId = '-1'

    this.actualTransactionData = await this.getActualTransactionDetail(this.materialCode, lineId, +this.budgetData.costCtrId, fromDate!, toDate!)
    this.costCenterData = await this.getCostCenterByLineId(lineId)

  }

  async onSectionChange(event: any) {
    const costCtrId = +event.target.value
    this.budgetData.costCtrId = `${costCtrId}`

    const transformDate = (date: Date) => this.datePipe.transform(date, 'yyyy-MM-dd')
    const [fromDate, toDate] = [transformDate(this.dateRange.from), transformDate(this.dateRange.to)]
    
    this.actualTransactionData = await this.getActualTransactionDetail(this.materialCode, +this.budgetData.lineId, +this.budgetData.costCtrId, fromDate!, toDate!)
  }

  setActualBudgetPercentage(budget: number, actual: number): number {
    return ((actual / budget) * 100);
  }

  openModal(template: any, data: any) {
    this.modalData = {...data}
    Object.keys(this.modalData).forEach(key => {
      if (key.includes("id") || key.includes("is_removed") || key.includes("created_at") || key.includes("updated_at")) {
        delete this.modalData[key]
      }
      if (key.includes("date") && key !== "updated_at") {
        this.modalData[key] = this.common.getLocaleDate(this.modalData[key])
      }
      if (key.includes("price")) {
        this.modalData[key] = this.common.getRupiahFormat(+this.modalData[key])
      }
    })
    this.modalKeys = Object.keys(this.modalData).map(str => {
      return { key: str, title: str.split('_').map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(' ')}
    })
    this.modalService.open(template, { centered: true, size: 'lg' })
  }

  async onYearChange() {
    let budgetIdChange = this.budgetId.replace(/^(\d+)-/, this.budgetData.year + "-")
    this.budgetId = budgetIdChange
    this.mergeBudgetActualData = await this.getMergeBudgetActualDetail(this.budgetId)
  }

  async onUsageSectionChange(event: any) {
    const selectedData = JSON.parse(this.selectedUsageSection)
    const budgetIdSectionChange = this.budgetId.replace(/^(\d+)-(\d+)-(\d+)-(\d+)/, "$1-$2-"+ selectedData.cost_ctr_id +"-$4");
    const budgetIdLineChange = budgetIdSectionChange.replace(/^(\d+)-(\d+)-(\d+)-(\d+)/, "$1-"+ selectedData.line_id +"-$3-$4");
    this.budgetId = budgetIdLineChange
    this.mergeBudgetActualData = await this.getMergeBudgetActualDetail(this.budgetId)
  }

  showSectionName(): string {
    const selectedData = JSON.parse(this.selectedUsageSection)
    const selectedSection = this.usageSectionData.find(item => {
      return item.line_id === selectedData.line_id
    }).sections.find((item: any) => item.id === selectedData.cost_ctr_id).section
    return selectedSection
  }
}
