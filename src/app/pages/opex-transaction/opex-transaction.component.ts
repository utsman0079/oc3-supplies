import { HttpErrorResponse } from '@angular/common/http';
import { Component, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, OperatorFunction, Subject, debounceTime, distinctUntilChanged, of, switchMap, tap } from 'rxjs';
import { CommonService } from 'src/app/core/services/common.service';
import { restApiService } from 'src/app/core/services/rest-api.service';
import { TokenStorageService } from 'src/app/core/services/token-storage.service';
import { Const } from 'src/app/core/static/const';
import { GlobalComponent, PriceType, UploadType } from 'src/app/global-component';

type RefreshType = 'Line' | 'Section' | 'Year'

@Component({
  selector: 'app-opex-transaction',
  templateUrl: './opex-transaction.component.html',
  styleUrls: ['./opex-transaction.component.scss']
})
export class OpexTransactionComponent {
  tabData = [{id: 1, name: 'Budget'}, {id: 2, name: 'Actual'}, {id: 3, name: 'Comparison'}]
  tableColumns: string[] = []
  tableFootPrice: number[] = []
  isLoading = false;

  opexTrData: any[] = [];
  opexTrDataBefore: any[] = [];
  opexTrDataBeforeByTab: any[] = [];
  lineData: any[] = [];
  costCenterData: any[] = [];

  year: number = new Date().getFullYear()
  
  selectedCostCenter = {
    id: 0,
    section: ''
  }

  selectedLine = {
    id: 0,
    line: ''
  }

  uploadedFiles: File | null = null;
  uploadedFilename: string | null = null
  searchKeyword = ''

  err: { dupplicateOpex?: any[], notFoundOpex?: any[] } = {
    dupplicateOpex: [],
    notFoundOpex: []
  }

  breadCrumbItems!: Array<{}>
  sortedColumn: string = '';
  isAscending: boolean = false;

  userData: any

  isTabOpen = {
    budget: true,
    actual: false,
    comparison: false
  }
  activeTab = 1

  activeTabImport: UploadType = 'create'

  opexXlsxLink = (priceType: PriceType, year: number, costCtrId: number) => {
    return `${GlobalComponent.API_URL}file/xlsx/opex-tr/${priceType}/${year}/${costCtrId}` 
  }
  opexXlsxTemplateLink = `${GlobalComponent.API_URL}file/xlsx/opex-tr-template`

  refreshSubject = new Subject<RefreshType>()

  searchLength: number | null = null
  opexMasterSearching = false;
  selectedOpexMaster: any
  opexMasterFormatter = (item: any) => item.order ? `${item.order} - ${item.name}` : ``
  searchOpexMaster: OperatorFunction<string, any[]> = (text$: Observable<any>) =>
    text$.pipe(debounceTime(300), distinctUntilChanged(), tap(() => (this.opexMasterSearching = true)), switchMap((term: string) => {
      if (term.length >= 3) {
        this.searchLength = null;
        return this.apiService.searchOpexMaster(term)
      } else {
        this.searchLength = term.length;
        return of([] as any[])
      }
    }),
    tap(() => this.opexMasterSearching = false)
  )

  formBudgetMonth: any[] = []
  emptyFormBudgetMonth: any[] = []
  isEditMode = false;

  @ViewChild('importDetailModal') importDetailModal: any

  constructor(
    private apiService: restApiService, 
    public common: CommonService, 
    public modalService: NgbModal,
    private tokenService: TokenStorageService,
    private router: Router, 
    private route: ActivatedRoute
  ) {
    this.userData = this.tokenService.getUser();
    this.year = new Date().getFullYear();
    this.breadCrumbItems = [
      { label: 'OPEX Budget', active: true }
    ]

    this.refreshSubject.pipe(debounceTime(350)).subscribe(async refreshValue => {
      if (refreshValue === 'Line') {
        this.costCenterData = await this.getCostCenterByLineId(this.selectedLine.id)
      }
      this.opexTrData = await this.getOpexTrByYearAndCostCtrId(this.year, this.selectedCostCenter.id)
      this.isEditMode = false;
    })
  }

  async ngOnInit() {
    this.lineData = await this.getFactoryLine()
    
    const query = this.route.snapshot.queryParams;

    this.setFormBudgetMonths()

    if (Object.keys(query).length > 0) {
      if (query['lineId']) {
        this.selectedLine.id = +query['lineId']
        this.costCenterData = await this.getCostCenterByLineId(this.selectedLine.id)
      }
      if (query['year']) this.year = +query['year']
      if (query['costCtrId']) this.selectedCostCenter.id = +query['costCtrId']
      if (query['tab']) {
        this.isTabOpen = {
          budget: query['tab'] === 'Budget',
          actual: query['tab'] === 'Actual',
          comparison: query['tab'] === 'Comparison'
        }
        this.activeTab = query['tab'] === 'Comparison' ? 3 : query['tab'] === 'Actual' ? 2 : 1

      }
      this.isLoading = true
      this.refreshSubject.next(this.costCenterData.length > 0 ? 'Section' : 'Line')
    } else {
      this.costCenterData = await this.getCostCenterByLineId(this.selectedLine.id)
      this.opexTrData = await this.getOpexTrByYearAndCostCtrId(this.year, this.selectedCostCenter.id)
    }
  }

  async getOpexTrByYearAndCostCtrId(year: number, costCtrId: number) {
    return new Promise<any[]>((resolve, reject) => {
      this.isLoading = true;
      this.apiService.getOpexTrByYearAndCostCtrId(year, costCtrId).subscribe({
        next: (res) => {
          this.isLoading = false;
          let data: any[] = (res.data as any[]).map((item) => {
            let chartOfAccount = `${item.order} - ${item.opex}`
            let months: { [key: string]: {tr_opex_id: number, price: number, isActual: boolean} } = {}
            item.budgeting_data.forEach((price: any) => {
              months[`${this.common.getSimpleMonthName(price.month)} Budget`] = {
                tr_opex_id: price.tr_opex_id, 
                price: price.budget,
                isActual: false
              }
              months[`${this.common.getSimpleMonthName(price.month)} Actual`] = {
                tr_opex_id: price.tr_opex_id, 
                price: price.actual,
                isActual: true
              }
            })
            return { is_selected: false, budget_id: item.budget_id, 'Chart Of Account': chartOfAccount, ...months }
          })
          const opexTrData = this.transformOpexTrData(data)
          this.opexTrDataBefore = data.map(item => ({...item}))
          resolve(opexTrData)
        },
        error: (err) => {
          this.isLoading = false;
          this.common.showServerErrorAlert(Const.ERR_GET_MSG('Opex Transaction'), err)
          reject(err)
        }
      })
    })
  }

  async getFactoryLine() {
    return new Promise<any[]>((resolve, reject) => {
      this.isLoading = true;
      this.apiService.getFactoryLine().subscribe({
        next: (res) => {
          this.isLoading = false;
          let data: any[] = res.data
          const userLineAccess: any[] = JSON.parse(this.userData.line_access)
          if (!userLineAccess.some(access => access.name === "All Access Granted")) {
            data = [...data].filter(line => userLineAccess.some(access => access.id === line.id))
              .map(item=> ({ id: item.id, name: item.name }))
          }
          this.selectedLine = { id: data[0].id, line: data[0].name }
          resolve(data)
        },
        error: (err) => {
          this.isLoading = false;
          this.common.showServerErrorAlert(Const.ERR_GET_MSG('Factory Line'), err)
          reject(err)
        }
      })
    })
  }

  async getCostCenterByLineId(lineId: number) {
    return new Promise<any[]>((resolve, reject) => {
      this.isLoading = true;
      this.apiService.getCostCenterByLineId(lineId).subscribe({
        next: (res) => {
          this.isLoading = false;
          this.selectedCostCenter = { id: res.data[0].id, section: res.data[0].section}
          resolve(res.data)
        },
        error: (err) => {
          this.isLoading = false;
          this.common.showServerErrorAlert(Const.ERR_GET_MSG('Cost Center'), err)
          reject(err)
        }
      })
    })
  }

  async updateMultipleOpexTr(data: any) {
    return new Promise<boolean>((resolve, reject) => {
      this.isLoading = true;
      this.apiService.updateMultipleOpexTr(data).subscribe({
        next: (res) => {
          this.isLoading = false;
          resolve(true)
        },
        error: (err) => {
          this.isLoading = false;
          this.common.showErrorAlert(Const.ERR_UPDATE_MSG('Opex Transaction'), err)
          reject(err)
        }
      })
    })
  }

  transformOpexTrData(opexTrData: any[]): any[] {
    if (opexTrData.length < 1) {
      return opexTrData
    }
    opexTrData.forEach(item => delete item["Total Value"])
    const data = opexTrData.map(item => {
      let months = [1,2,3,4,5,6,7,8,9,10,11,12]
      if (!this.isTabOpen.comparison) {
        let newItem = {...item}
        let price = 0
        months.forEach(month => {
          delete newItem[`${this.common.getSimpleMonthName(month)} ${!this.isTabOpen.actual ? 'Actual' : 'Budget'}`]
          newItem[`${this.common.getSimpleMonthName(month)}`] = newItem[`${this.common.getSimpleMonthName(month)} ${this.isTabOpen.actual ? 'Actual' : 'Budget'}`]
          delete newItem[`${this.common.getSimpleMonthName(month)} ${this.isTabOpen.actual ? 'Actual' : 'Budget'}`]
          price += newItem[`${this.common.getSimpleMonthName(month)}`].price
        })
        newItem['Total Value'] = price
        return newItem
      } else {
        let price = 0
        months.forEach(month => {
          price += item[`${this.common.getSimpleMonthName(month)} Budget`].price
          price += item[`${this.common.getSimpleMonthName(month)} Actual`].price
        })
        item['Total Value'] = price
        return item
      }
    })
    this.tableColumns = Object.keys(data[0]).filter(item => !item.includes('budget_id') && !item.includes('is_selected'))

    const months = this.tableColumns.filter(item => !item.includes('Chart Of Account') && !item.includes('Total Value'))

    const filterredMonthData = data.map(item => {
      const newItem: any = {} = {...item}
      delete newItem['Chart Of Account']
      delete newItem['Total Value']
      delete newItem['budgetId']
      return newItem
    })

    this.tableFootPrice = filterredMonthData.reduce((acc, opex) => {
      months.forEach((month, i) => acc[i] += opex[month].price)
      return acc
    }, [...months].map(price => 0))

    if (!this.isTabOpen.budget) {
      this.activeTabImport = 'update'
    } else {
      this.activeTabImport = 'create'
    }

    this.opexTrDataBeforeByTab = JSON.parse(JSON.stringify(data))
    return data
  }

  onFactoryLineChange(event: any) {
    if (event.target.value) {
      this.router.navigate([], { queryParams: { lineId: +event.target.value } })
      const selectedLine = this.lineData.find(line => line.id === +event.target.value)
      this.selectedLine = {
        id: selectedLine.id,
        line: selectedLine.name
      }
      this.refreshSubject.next("Line")
    }
  }

  onButtonChangeYear(action: string) {
    if (action === "next") this.year += 1
    if (action === "prev") this.year -= 1
    this.router.navigate([], { queryParams: { year: this.year }, queryParamsHandling: 'merge' })
    this.refreshSubject.next("Year")
  }

  onYearChange(event: any) {
    if (event.target.value) {
      this.router.navigate([], { queryParams: { year: this.year }, queryParamsHandling: 'merge' })
      this.refreshSubject.next("Year")
    }
  }

  async openUpdateModal(content: TemplateRef<any>, data?: any) {
    if (data) {

    }
    this.modalService.open(content, { size: 'xl', centered: true }).result.then(
      (result) => this.resetModalValue(),
      (reason) => this.resetModalValue()
    )
  }

  resetModalValue() {
    this.uploadedFiles = null
    this.uploadedFilename = null;
    this.activeTabImport = this.isTabOpen.actual ? 'update' : 'create'
    this.selectedOpexMaster = undefined;
    this.formBudgetMonth = this.emptyFormBudgetMonth.map(i => ({...i}))
  }

  onDeleteOpexTransaction() {
    let selectedCount = 0
    this.opexTrData.forEach(item => {
      if (item.is_selected) selectedCount += 1
    })
    if (selectedCount > 0) {
      this.common.showDeleteWarningAlert(Const.ALERT_DEL_MSG(`${selectedCount} OPEX`)).then(async (result) => {
        if (result.value) {
          const removeData: any[] = []
          this.opexTrData.forEach(opex => {
            if (opex.is_selected) {
              removeData.push({ budget_id: opex.budget_id, data: { is_removed: 1 } })
            }
          })
          if (removeData.length > 0) {
            await this.updateMultipleOpexTr(removeData).then(() => {
              this.refreshSubject.next('Section')
              if (this.isTabOpen.budget) {
                this.activeTabImport = 'create'
              } else {
                this.activeTabImport = 'update'
              }
            })
          }
        }
      })
    }
  }

  onSectionChange(event: any) {
    const selectedSection = this.costCenterData.find(costCenter => costCenter.id === +event.target.value)
    this.selectedCostCenter = {
      id: selectedSection.id,
      section: selectedSection.section
    }
    this.router.navigate([], { queryParams: { costCtrId: +event.target.value }, queryParamsHandling: 'merge' })
    this.refreshSubject.next("Section")
  }

  onTabChange(event: any) {
    this.isEditMode = false;
    const selectedTab = JSON.parse(event.target.name)
    this.isTabOpen = {
      budget: selectedTab.name === "Budget",
      actual: selectedTab.name === "Actual",
      comparison: selectedTab.name === "Comparison"
    }
    this.opexTrData = this.transformOpexTrData(this.opexTrDataBefore)
    if (!this.isTabOpen.budget) {
      this.activeTabImport = 'update'
    } else {
      this.activeTabImport = 'create'
    }
    this.router.navigate([], { queryParams: { tab: selectedTab.name }, queryParamsHandling: 'merge' })
  }

  columnData(item: any): string {
    if (typeof item !== 'string') {
      if (typeof item === 'object') {
        return this.common.getRupiahFormat(item.price)
      } else {
        return this.common.getRupiahFormat(item)
      }
    }
    return item
  }

  sort(col: string) {
    if (this.sortedColumn === col) {
      this.isAscending = !this.isAscending
    } else {
      this.sortedColumn = col
      this.isAscending = true
    }

    this.opexTrData.sort((a, b) => {
      if (typeof a[col] === 'string' && typeof b[col] === 'string') {
        return this.isAscending ? a[col].localeCompare(b[col]) : b[col].localeCompare(a[col])
      } else if (typeof a[col] === 'number' && typeof b[col] === 'number') {
        return this.isAscending ? a[col] - b[col] : b[col] - a[col]
      } else {
        return this.isAscending ? a[col].price - b[col].price : b[col].price - a[col].price
      }
    })
  }

  opexTransaction(): any[] {
    return this.opexTrData.filter((data) => 
      data['Chart Of Account'].toLowerCase().includes(this.searchKeyword.trim().toLocaleLowerCase())
    )
  }

  actualBudgetPercentage(budget: number, actual: number): number {
    return ((actual / budget) * 100);
  }

  onXLSXFileChange(event: any) {
    this.uploadedFiles = event.target.files[0]
  }

  onXlsxImport() {
    if (this.uploadedFiles) {
      this.isLoading = true
      const formData = new FormData()
      formData.append('file', this.uploadedFiles)

      const priceType: PriceType = this.isTabOpen.actual ? 'actual' : 'budget'
      this.isLoading = true
      this.apiService.uploadOpexTrXlsx(priceType, this.activeTabImport, this.selectedCostCenter.id, this.year, formData).subscribe({
        next: (res: any) => {
          this.isLoading = false
          this.modalService.dismissAll()
          this.common.showSuccessAlert(res.data.message)
          this.refreshSubject.next('Section')
        },
        error: (err: HttpErrorResponse) => {
          this.isLoading = false
          this.modalService.dismissAll()
          let res = err.error.data
          if (err.status === 400) {
            this.common.showCustomAlert(
              `Operation Cancelled`,
              `${res.message}`,
              res.missing_columns || res.message.includes("not found") || res.message.includes("Invalid year") ? 'Close' : 'See Details'
            ).then((result) => {
              if (result.value && res.message.includes("duplicates")) {
                this.err = {
                  dupplicateOpex: res.duplicates_opex || [],
                  notFoundOpex: res.not_included_opex || []
                }
                this.modalService.open(this.importDetailModal, { centered: true, scrollable: true })
              }
            })
          } else {
            this.common.showErrorAlert(Const.ERR_INSERT_MSG("Opex Xlsx"), err.statusText)
          }
        }
      })
    }
  }

  onImportModalNavChange() {
    this.uploadedFiles = null
    this.uploadedFilename = null
  }

  onChecklistAll(event: any) {
    this.opexTrData.forEach(opex => event.target.checked ? opex.is_selected = true : opex.is_selected = false)
  }

  onCheckedSupplyBudget(event: any) {
    const condition = event.target.checked
    const budgetId = event.target.value
    const index = this.common.getIndexById(this.opexTrData, budgetId, "budget_id")
    this.opexTrData[index].is_selected = condition
  }

  setFormBudgetMonths() {
    this.formBudgetMonth.splice(0)
    for (let i = 1; i <= 12; i++) {
      this.formBudgetMonth.push({
        number: i,
        month: this.common.getSimpleMonthName(i),
        price: 0
      })
    }
    this.emptyFormBudgetMonth = this.formBudgetMonth.map(i => ({...i}))
  } 

  onEditSaveChanges() {
    const form: any[] = []

    this.opexTrData.forEach((after) => {
      const before = this.opexTrDataBeforeByTab.find(before => before.budget_id === after.budget_id);
      if (before) {
        for (let m = 1; m <= 12; m++) {
          const month = this.common.getSimpleMonthName(m)
          if (after[month].price !== before[month].price) {
            const price = after[month].isActual ? 'actual' : 'budget'
            form.push({
              id: after[month].tr_opex_id,
              data: {
                [price]: after[month].price
              }
            })
          }
        }
      }
    })

    if (form.length > 0) {
      this.isLoading = true;
      this.apiService.updateMultipleOpexTrById(form).subscribe({
        next: (res) => {
          this.refreshSubject.next("Section")
        },
        error: (err) => {
          this.isLoading = false;
          this.common.showErrorAlert(Const.ERR_UPDATE_MSG("Opex Transaction"), err)
        }
      })
    }
    
  }

  onSaveChanges() {
    if (this.selectedOpexMaster) {
      this.isLoading = true;
      const budgetId = `${this.year}-${this.selectedLine.id}-${this.selectedCostCenter.id}-${this.selectedOpexMaster.id}`
      
      this.apiService.checkOpexTrBudgetIdAvailability(budgetId).subscribe({
        next: (res) => {
          const form: any[] = []
          this.formBudgetMonth.forEach(item => {
            form.push({
              month: item.number,
              budget_id: budgetId,
              year: this.year,
              cost_ctr_id: this.selectedCostCenter.id,
              opex_id: this.selectedOpexMaster.id,
              budget: this.isTabOpen.budget ? +item.price || 0 : 0,
              actual: this.isTabOpen.actual ? +item.price || 0 : 0,
            })
          })

          this.apiService.insertOpexTr(form).subscribe({
            next: (_res) => {
              this.isLoading = false;
              this.modalService.dismissAll()
              this.refreshSubject.next('Section')
            },
            error: (_err) => {
              this.isLoading = false;
              this.common.showErrorAlert(Const.ERR_INSERT_MSG("Opex Transaction"), _err)
            }
          })
        },
        error: (err: HttpErrorResponse) => {
          this.isLoading = false;
          if (err.status === 400) {
            this.common.showErrorAlert(err.error.data.message, "Failed")
          } else {
            this.common.showErrorAlert(Const.ERR_GET_MSG("Check Opex Budget Id"), err.statusText)
          }
        }
      })

    }
    
  }

  onEditModeChecked(event: any) {
    this.isLoading = true;
    setTimeout(() => {
      this.isEditMode = event.target.checked
      this.opexTrData = JSON.parse(JSON.stringify(this.opexTrDataBeforeByTab))
      this.isLoading = false;
    }, 50)
    
  }
  
}
