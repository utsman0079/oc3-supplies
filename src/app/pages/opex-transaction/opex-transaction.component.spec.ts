import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OpexTransactionComponent } from './opex-transaction.component';

describe('OpexTransactionComponent', () => {
  let component: OpexTransactionComponent;
  let fixture: ComponentFixture<OpexTransactionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OpexTransactionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OpexTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
